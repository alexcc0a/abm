package com.nesterov;

public class Main {
    public static int search(String text, String pattern) {
        int n = text.length(); // Длина исходной строки.
        int m = pattern.length(); // Длина подстроки.

        int[] last = new int[256]; // Массив для хранения последнего вхождения каждого символа.
        for (int i = 0; i < 256; i++) {
            last[i] = -1; // Инициализация массива значением -1.
        }
        for (int i = 0; i < m; i++) {
            last[(int) pattern.charAt(i)] = i; // Заполнение массива последними вхождениями символов из подстроки.
        }

        int i = m - 1; // Индекс текущего символа в исходной строке.
        int j = m - 1; // Индекс текущего символа в подстроке.
        while (i < n) { // Пока не достигнут конец исходной строки.
            if (text.charAt(i) == pattern.charAt(j)) { // Если символы совпадают.
                if (j == 0) { // Если вся подстрока найдена.
                    return i; // Возвращаем индекс начала подстроки в исходной строке.
                }
                i--; // Переходим к следующему символу в обеих строках.
                j--;
            } else { // Если символы не совпадают.
                i += m - Math.min(j, 1 + last[(int) text.charAt(i)]); // Сдвигаем индекс в исходной строке.
                j = m - 1; // Возвращаем индекс в конец подстроки.
            }
        }

        return -1; // Если подстрока не найдена, возвращаем -1.
    }

    public static void main(String[] args) {
        String text = "cnalsksadc"; // Исходная строка.
        String pattern = "lsk"; // Подстрока.

        int index = search(text, pattern); // Поиск подстроки в строке.

        if (index != -1) {
            System.out.println("Pattern found at index: " + index); // Если подстрока найдена, выводим индекс начала подстроки.
        } else {
            System.out.println("Pattern not found"); // Если подстрока не найдена, выводим сообщение.
        }
    }
}